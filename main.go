package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/common/cosmos"
)

func setupConfig() {
	// Read in the configuration file for the sdk
	nw := common.GetCurrentChainNetwork()
	switch nw {
	case common.TestNet:
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("tthor", "tthorpub")
		config.SetBech32PrefixForValidator("tthorv", "tthorvpub")
		config.SetBech32PrefixForConsensusNode("tthorc", "tthorcpub")
		config.Seal()
	case common.MainNet:
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("thor", "thorpub")
		config.SetBech32PrefixForValidator("thorv", "thorvpub")
		config.SetBech32PrefixForConsensusNode("thorc", "thorcpub")
		config.Seal()
	}
}

func main() {
	setupConfig()
	app := &cli.App{
		Name:  "supertools",
		Usage: "this tool provide means to get information from thorchain",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:       "server",
				Aliases:    []string{"s"},
				Usage:      "server url or ip address",
				EnvVars:    []string{"SERVER_URL"},
				Value:      "localhost",
				Required:   true,
				Hidden:     false,
				HasBeenSet: true,
			},
			&cli.IntFlag{
				Name:       "port",
				Aliases:    []string{"p"},
				Usage:      "port",
				EnvVars:    []string{"SERVER_PORT"},
				Value:      1317,
				HasBeenSet: true,
			},
			&cli.StringFlag{
				Name:     "binance",
				Aliases:  []string{"b"},
				Usage:    "binance server url host:port",
				EnvVars:  []string{"BINANCE_URL"},
				Required: false,
				Hidden:   false,
			},
			&cli.StringFlag{
				Name:        "format",
				Aliases:     []string{"f"},
				Usage:       "output format, you can choose json or plain",
				DefaultText: "plain",
			},
		},
		Commands: cli.Commands{
			getAsgardCmd(),
			getYggdrasilCmd(),
			getNodes(),
			getPoolStakers(),
			getBitcoinCmd(),
			getTxVoterCmd(),
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
