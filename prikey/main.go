package main

import (
	"encoding/hex"
	"fmt"

	"github.com/btcsuite/btcd/btcec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/tendermint/tendermint/crypto/secp256k1"
)

func main() {
	config := sdk.GetConfig()
	// thorchain will import go-tss as a library , thus this is not needed, we copy the prefix here to avoid go-tss to import thorchain
	config.SetBech32PrefixForAccount("thor", "thorpub")
	config.SetBech32PrefixForValidator("thorv", "thorvpub")
	config.SetBech32PrefixForConsensusNode("thorc", "thorcpub")

	input := "9294f4d108465fd293f7fe299e6923ef71a77f2cb1eb6d4394839c64ec25d5c0"
	buf, err := hex.DecodeString(input)
	if err != nil {
		panic(err)
	}
	pkey, _ := btcec.PrivKeyFromBytes(btcec.S256(), buf)
	pubkeyBytes := pkey.PubKey().SerializeCompressed()

	var pubKeyCompressed secp256k1.PubKeySecp256k1
	copy(pubKeyCompressed[:], pubkeyBytes)
	pubKey, err := sdk.Bech32ifyPubKey(sdk.Bech32PubKeyTypeAccPub, pubKeyCompressed)
	addr := sdk.AccAddress(pubKeyCompressed.Address().Bytes())
	fmt.Println("pubkey:", pubKey)
	fmt.Println("address:", addr.String())

}
