module gitlab.com/thorchain/heimdall/tools/prikey

go 1.14

require (
	github.com/btcsuite/btcd v0.20.1-beta
	github.com/cosmos/cosmos-sdk v0.39.1
	github.com/tendermint/tendermint v0.33.8
)
