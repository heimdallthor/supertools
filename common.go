package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

func getFromThorchain(serverIP string, port int, path string) ([]byte, error) {
	url := fmt.Sprintf("http://%s:%d/thorchain/%s", serverIP, port, path)
	client := http.Client{
		Timeout: time.Second * 3,
	}
	resp, err := client.Get(url)
	if err != nil {
		return nil, fmt.Errorf("fail to call %s :%w", url, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("not expected status : %s", resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("fail to read response body: %w", err)
	}
	return body, nil
}
