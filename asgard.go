package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/logrusorgru/aurora/v3"
	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/x/thorchain/types"
)

func getAsgardCmd() *cli.Command {
	return &cli.Command{
		Name:        "asgard",
		Aliases:     []string{"a"},
		Usage:       "this command is used to check all the information related to asgard",
		UsageText:   "",
		Description: "",
		ArgsUsage:   "",
		Category:    "vaults",
		Action:      asgardCmdAction,
	}
}
func asgardCmdAction(ctx *cli.Context) error {
	serverIP := ctx.String("server")
	if serverIP == "" {
		return errors.New("no server ip")
	}
	port := ctx.Int("port")
	if port == 0 {
		port = 1317
	}
	binanceRPC := ctx.String("binance")
	if binanceRPC == "" {
		return errors.New("binance RPC server is empty")
	}
	vaults, err := getAsgard(serverIP, port)
	if err != nil {
		return fmt.Errorf("fail to get asgard infor from %s:%d, %w", serverIP, port, err)
	}
	format := ctx.String("format")
	if strings.EqualFold(format, "json") {
		buf, err := json.MarshalIndent(vaults, " ", "")
		if err != nil {
			return fmt.Errorf("fail to marshal vaults to json: %w", err)
		}
		fmt.Println(string(buf))
		return nil
	}

	for _, vault := range vaults {
		tw := tablewriter.NewWriter(os.Stdout)
		tw.SetBorder(true)
		tw.SetRowLine(true)
		tw.Append([]string{"vault created at: ", strconv.FormatInt(vault.BlockHeight, 10)})
		tw.Append([]string{"vault public key: ", vault.PubKey.String()})
		if vault.Status == types.ActiveVault {
			tw.Append([]string{"vault status:", aurora.Green("active").String()})
		} else {
			tw.Append([]string{"vault status:", aurora.Red(vault.Status).String()})
		}
		tw.Append([]string{"status since: ", strconv.FormatInt(vault.StatusSince, 10)})
		tw.Append([]string{"inbound tx count: ", strconv.FormatInt(vault.InboundTxCount, 10)})
		tw.Append([]string{"outbound tx cound", strconv.FormatInt(vault.OutboundTxCount, 10)})
		tw.Append([]string{"vault members: ", vault.Membership.String()})
		// tw.Append([]string{"coins: ", getCoinForDisplay(vault.Coins)})
		tw.Append([]string{"signing party:", vault.SigningParty.String()})
		tw.Append([]string{"pending tx:", fmt.Sprintf("%+v", vault.PendingTxBlockHeights)})
		tw.Render()
		addr, err := vault.PubKey.GetAddress(common.BNBChain)
		if err != nil {
			fmt.Printf("fail to get vault BNB address: %s\n", err.Error())
			continue
		}
		account, err := GetAccountByAddress(binanceRPC, addr.String())
		if err != nil {
			fmt.Printf("fail to get account from Binance,err:%s\n", err.Error())
			continue
		}
		if err := printVault(ctx, vault.Coins, account.Coins); err != nil {
			fmt.Printf("fail to print vault, err: %s\n", err.Error())
			continue
		}
	}

	return nil
}

func printVault(ctx *cli.Context, vaultCoins common.Coins, accountCoins common.AccountCoins) error {
	tw := tablewriter.NewWriter(os.Stdout)
	tw.SetBorder(true)
	tw.SetRowLine(true)
	tw.SetHeader([]string{
		"coin",
		"vault",
		"wallet",
		"diff",
		"status",
	})
	for _, c := range vaultCoins {
		walletCoin := common.AccountCoin{}
		for _, ac := range accountCoins {
			if strings.EqualFold(c.Asset.Symbol.String(), ac.Denom) {
				walletCoin = ac
			}
		}
		status := aurora.Green("OK").String()
		if c.Amount.Uint64() != walletCoin.Amount {
			status = aurora.Red("FAIL").String()
		}
		diff := "0"
		if walletCoin.Amount > c.Amount.Uint64() {
			gap := walletCoin.Amount - c.Amount.Uint64()
			diff = aurora.Green(fmt.Sprintf("+%s", strconv.FormatFloat(float64(gap)/float64(common.One), 'f', -1, 64))).String()
		}
		if walletCoin.Amount < c.Amount.Uint64() {
			gap := c.Amount.Uint64() - walletCoin.Amount
			diff = aurora.Red(fmt.Sprintf("-%s", strconv.FormatFloat(float64(gap)/float64(common.One), 'f', -1, 64))).String()
		}
		tw.Append([]string{
			c.Asset.String(),
			strconv.FormatFloat(float64(c.Amount.Uint64())/float64(common.One), 'f', -1, 64),
			strconv.FormatFloat(float64(walletCoin.Amount)/float64(common.One), 'f', -1, 64),
			diff,
			status,
		})
	}
	tw.Render()
	return nil
}

func getCoinForDisplay(coins common.Coins) string {
	var result []string
	sort.SliceStable(coins, func(i, j int) bool {
		return coins[i].Asset.String() < coins[j].Asset.String()
	})
	for _, c := range coins {
		result = append(result, fmt.Sprintf("%s %s", c.Asset.String(), strconv.FormatFloat(float64(c.Amount.Uint64())/float64(common.One), 'f', -1, 64)))
	}
	return strings.Join(result, " ")
}
func getAsgard(serverIP string, port int) (types.Vaults, error) {
	buf, err := getFromThorchain(serverIP, port, "vaults/asgard")
	if err != nil {
		return types.Vaults{}, nil
	}
	var vaults types.Vaults
	if err := codec.Cdc.UnmarshalJSON(buf, &vaults); err != nil {
		return nil, fmt.Errorf("fail to unmarshal vaults: %w", err)
	}
	return vaults, nil
}
