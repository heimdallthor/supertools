package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"

	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/common/cosmos"
	"gitlab.com/thorchain/thornode/x/thorchain/types"
)

func getYggdrasilCmd() *cli.Command {
	return &cli.Command{
		Name:        "yggdrasil",
		Aliases:     []string{"a"},
		Usage:       "this command is used to check all the information related to asgard",
		UsageText:   "",
		Description: "",
		ArgsUsage:   "",
		Category:    "vaults",
		Action:      yggdrasilCmdAction,
	}
}

func yggdrasilCmdAction(ctx *cli.Context) error {
	serverIP := ctx.String("server")
	if serverIP == "" {
		return errors.New("no server ip")
	}
	port := ctx.Int("port")
	if port == 0 {
		port = 1317
	}
	yggdrasilVaults, err := getYggdrasil(serverIP, port)
	if err != nil {
		return fmt.Errorf("fail to get yggdrasil vaults: %w", err)
	}
	binanceURL := ctx.String("binance")
	return printYggdrasilVaults(yggdrasilVaults, binanceURL)
}

func printYggdrasilVaults(vaults []types.QueryYggdrasilVaults, binanceURL string) error {
	tw := tablewriter.NewWriter(os.Stdout)
	tw.SetRowLine(true)
	tw.SetBorder(true)
	tw.SetHeader([]string{
		"pub key",
		"BNB address",
		"inbound",
		"outbound",
		"bond",
		"total value",
		"status",
		"coins in vault",
		"coins in wallet",
		"solvent",
	})
	for _, v := range vaults {
		row, err := printYggdrasilVault(v, binanceURL)
		if err != nil {
			fmt.Println("fail to get yggdrasil vault information:", fmt.Sprintf("%+v", err))
		}
		tw.Append(row)
	}
	tw.Render()
	return nil
}

func printYggdrasilVault(vault types.QueryYggdrasilVaults, binanceURL string) ([]string, error) {
	bnbAddr, err := vault.Vault.PubKey.GetAddress(common.BNBChain)
	if err != nil {
		return nil, fmt.Errorf("fail to get binance address: %w", err)
	}
	account, err := GetAccountByAddress(binanceURL, bnbAddr.String())
	if err != nil {
		return nil, fmt.Errorf("fail to get account from binance(%s),err: %w", binanceURL, err)
	}
	solvent := true
	var coins common.Coins
	for _, c := range account.Coins {
		asset, err := common.NewAsset("BNB." + c.Denom)
		if err != nil {
			return nil, fmt.Errorf("fail to parse asset:%w", err)
		}
		coins = append(coins, common.NewCoin(asset, cosmos.NewUint(c.Amount)))
		cExist := vault.Vault.Coins.GetCoin(asset)
		if cExist.Amount.Uint64() != c.Amount {
			solvent = false
		}
	}

	return []string{
		vault.Vault.PubKey.String(),
		bnbAddr.String(),
		strconv.FormatInt(vault.Vault.InboundTxCount, 10),
		strconv.FormatInt(vault.Vault.OutboundTxCount, 10),
		getHumanReadableAmount(vault.Bond),
		getHumanReadableAmount(vault.TotalValue),
		getStatusForDisplay(vault.Status),
		getCoinForDisplay(vault.Vault.Coins),
		getCoinForDisplay(coins),
		getOnlineStatus(solvent),
	}, nil
}
func getYggdrasil(serverIP string, port int) ([]types.QueryYggdrasilVaults, error) {
	buf, err := getFromThorchain(serverIP, port, "vaults/yggdrasil")
	if err != nil {
		return nil, fmt.Errorf("fail to get yggdrasil vaults from thorchain: %w", err)
	}
	var yggdrasilVaults []types.QueryYggdrasilVaults
	if err := codec.Cdc.UnmarshalJSON(buf, &yggdrasilVaults); err != nil {
		return nil, fmt.Errorf("fail to unmarshal yggdrasil vaults")
	}
	return yggdrasilVaults, nil
}
