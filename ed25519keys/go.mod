module gitlab.com/thorchain/heimdall/tools/ed25519keys

go 1.15

require (
	github.com/99designs/keyring v1.1.3
	github.com/cosmos/cosmos-sdk v0.39.1
	github.com/cosmos/go-bip39 v0.0.0-20200817134856-d632e0d11689
	github.com/decred/dcrd/dcrec/edwards v1.0.0
)
