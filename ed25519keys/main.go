package main // import "gitlab.com/thorchain/heimdall/tools/ed25519keys"

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/binary"
	"errors"
	"fmt"
	"math/big"
	"strconv"
	"strings"

	"github.com/99designs/keyring"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/go-bip39"
	"github.com/decred/dcrd/dcrec/edwards"
	"github.com/tendermint/tendermint/crypto/ed25519"
)

const (
	ThorchainDefaultBIP39PassPhrase = "thorchain"
	BIP44Prefix                     = "44'/931'/"
	PartialPath                     = "0'/0/0"
	FullPath                        = BIP44Prefix + PartialPath
)

func i64(key []byte, data []byte) (IL [32]byte, IR [32]byte) {
	mac := hmac.New(sha512.New, key)
	// sha512 does not err
	_, _ = mac.Write(data)
	I := mac.Sum(nil)
	copy(IL[:], I[:32])
	copy(IR[:], I[32:])
	return
}

func uint32ToBytes(i uint32) []byte {
	b := [4]byte{}
	binary.BigEndian.PutUint32(b[:], i)
	return b[:]
}

func addScalars(a []byte, b []byte) [32]byte {
	aInt := new(big.Int).SetBytes(a)
	bInt := new(big.Int).SetBytes(b)
	sInt := new(big.Int).Add(aInt, bInt)
	x := sInt.Mod(sInt, edwards.Edwards().N).Bytes()
	x2 := [32]byte{}
	copy(x2[32-len(x):], x)
	return x2
}

func derivePrivateKey(privKeyBytes [32]byte, chainCode [32]byte, index uint32, harden bool) ([32]byte, [32]byte) {
	var data []byte
	if harden {
		index = index | 0x80000000
		data = append([]byte{byte(0)}, privKeyBytes[:]...)
	} else {
		// this can't return an error:
		_, ecPub, err := edwards.PrivKeyFromScalar(edwards.Edwards(), privKeyBytes[:])
		if err != nil {
			panic("it should not fail")
		}
		pubKeyBytes := ecPub.SerializeCompressed()
		data = pubKeyBytes
	}
	data = append(data, uint32ToBytes(index)...)
	data2, chainCode2 := i64(chainCode[:], data)
	x := addScalars(privKeyBytes[:], data2[:])
	return x, chainCode2
}

func DerivePrivateKeyForPath(privKeyBytes [32]byte, chainCode [32]byte, path string) ([32]byte, error) {
	data := privKeyBytes
	parts := strings.Split(path, "/")
	for _, part := range parts {
		// do we have an apostrophe?
		harden := part[len(part)-1:] == "'"
		// harden == private derivation, else public derivation:
		if harden {
			part = part[:len(part)-1]
		}
		idx, err := strconv.Atoi(part)
		if err != nil {
			return [32]byte{}, fmt.Errorf("invalid BIP 32 path: %s", err)
		}
		if idx < 0 {
			return [32]byte{}, errors.New("invalid BIP 32 path: index negative ot too large")
		}
		data, chainCode = derivePrivateKey(data, chainCode, uint32(idx), harden)
	}
	var derivedKey [32]byte
	n := copy(derivedKey[:], data[:])
	if n != 32 || len(data) != 32 {
		return [32]byte{}, fmt.Errorf("expected a (secp256k1) key of length 32, got length: %v", len(data))
	}

	return derivedKey, nil
}

func mnemonicToEddKey(mnemonic, masterSecret string) ([]byte, error) {
	words := strings.Split(mnemonic, " ")
	if len(words) != 12 && len(words) != 24 {
		return nil, errors.New("mnemonic length should either be 12 or 24")
	}
	seed, err := bip39.NewSeedWithErrorChecking(mnemonic, ThorchainDefaultBIP39PassPhrase)
	if err != nil {
		return nil, err
	}
	masterPriv, ch := i64([]byte(masterSecret), seed)
	derivedPriv, err := DerivePrivateKeyForPath(masterPriv, ch, FullPath)
	if err != nil {
		return nil, err
	}
	return derivedPriv[:], nil
}

func main() {
	db, err := keyring.Open(lkbToKeyringConfig("THORChain", ".thorcli", "password"))
	if err != nil {
		panic(err)
	}
	item, err := db.Get("thorchain-ed25519")
	if errors.Is(err, keyring.ErrKeyNotFound) {
		fmt.Println("thorchain-ed25519 not exist yet")
	} else {
		fmt.Println(item)
	}
	m2 := "bracket struggle enroll coral earth couple flight claw state brother suffer shallow auction reform amazing skirt size ostrich expect click shoe agree summer parent"
	derivedPrivkey, err := mnemonicToEddKey(m2, "")
	if err != nil {
		fmt.Printf("fail to generate the derived key\n")
		return
	}
	fmt.Printf("derived key: %v\n", derivedPrivkey)

	// now we test the ed25519 key can sign and verify
	_, pk, err := edwards.PrivKeyFromScalar(edwards.Edwards(), derivedPrivkey)
	if err != nil {
		panic(err)
	}
	fmt.Println(len(pk.Serialize()))

	var pkey ed25519.PubKeyEd25519
	copy(pkey[:], pk.Serialize())

	pubKey, err := sdk.Bech32ifyPubKey(sdk.Bech32PubKeyTypeAccPub, pkey)
	if err != nil {
		panic(err)
	}
	fmt.Println(pubKey)
	if err := db.Set(keyring.Item{
		Key:         "thorchain-ed25519",
		Data:        derivedPrivkey,
		Label:       "private key",
		Description: "private key for ed25519",
	}); err != nil {
		panic(err)
	}

}
func lkbToKeyringConfig(appName, dir string, password string) keyring.Config {
	return keyring.Config{
		ServiceName:     appName,
		AllowedBackends: []keyring.BackendType{keyring.FileBackend},
		FileDir:         dir,
		FilePasswordFunc: func(_ string) (string, error) {
			return password, nil
		},
	}
}
