package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/blang/semver"
	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/logrusorgru/aurora/v3"
	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/common/cosmos"
	"gitlab.com/thorchain/thornode/x/thorchain"
	"gitlab.com/thorchain/thornode/x/thorchain/types"
)

func getNodes() *cli.Command {
	return &cli.Command{
		Name:        "nodes",
		Aliases:     []string{"n"},
		Usage:       "this command is used to get all the nodes",
		UsageText:   "get all the node accounts from thorchain ",
		Description: "",
		ArgsUsage:   "",
		Category:    "nodes",
		Action:      nodeCmdAction,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "status",
				Usage:    "filter node account by status, it can be ALL,Active,Standby,Disabled",
				Required: false,
				Hidden:   false,
				Value:    "ALL",
			},
			&cli.BoolFlag{
				Name:       "request_to_leave",
				Aliases:    []string{"rl"},
				Required:   false,
				Hidden:     false,
				HasBeenSet: false,
			},
			&cli.BoolFlag{
				Name:       "force_to_leave",
				Aliases:    []string{"fl"},
				Required:   false,
				Hidden:     false,
				HasBeenSet: false,
			},
			&cli.StringFlag{
				Name:       "version",
				Required:   false,
				Hidden:     false,
				Value:      "",
				HasBeenSet: false,
			},
			&cli.StringSliceFlag{
				Name:       "pubkey",
				Aliases:    []string{"pkey"},
				Usage:      "filter by node public key",
				Required:   false,
				Hidden:     false,
				HasBeenSet: false,
			},
			&cli.BoolFlag{
				Name:       "slash",
				Usage:      "sort by slash point",
				Required:   false,
				Hidden:     false,
				Value:      false,
				HasBeenSet: false,
			},
		},
	}
}

type filter func(na types.QueryNodeAccount) bool

func nodeCmdAction(ctx *cli.Context) error {
	serverIP := ctx.String("server")
	if serverIP == "" {
		return errors.New("no server ip")
	}
	port := ctx.Int("port")
	if port == 0 {
		port = 1317
	}
	nodeAccounts, err := getNodeAccounts(serverIP, port)
	if err != nil {
		return fmt.Errorf("fail to get node accounts: %w", err)
	}
	status := ctx.String("status")
	if status != "ALL" {
		// filter by status
		nodeAccounts = filterNodeAccountsByStatus(nodeAccounts, status)
	}
	var filters []filter
	if ctx.IsSet("request_to_leave") {
		requestToLeave := ctx.Bool("request_to_leave")
		filters = append(filters, func(na types.QueryNodeAccount) bool {
			return na.RequestedToLeave == requestToLeave
		})
	}
	if ctx.IsSet("force_to_leave") {
		forceToLeave := ctx.Bool("force_to_leave")
		filters = append(filters, func(na types.QueryNodeAccount) bool {
			return na.RequestedToLeave == forceToLeave
		})
	}
	if ctx.IsSet("version") {
		version := ctx.String("version")
		filters = append(filters, func(na types.QueryNodeAccount) bool {
			return na.Version.GTE(semver.MustParse(version))
		})
	}
	if ctx.IsSet("pubkey") {
		pubkeys := ctx.StringSlice("pubkey")
		filters = append(filters, func(na types.QueryNodeAccount) bool {
			for _, pk := range pubkeys {
				if na.PubKeySet.Secp256k1.String() == pk {
					return true
				}
			}
			return false
		})
	}
	format := ctx.String("format")
	nodeAccounts = filterNodeAccountsByFilters(nodeAccounts, filters)
	if ctx.IsSet("slash") {
		sort.SliceStable(nodeAccounts, func(i, j int) bool {
			return nodeAccounts[i].SlashPoints > nodeAccounts[j].SlashPoints
		})
	}
	return printNodeAccounts(nodeAccounts, format)

}

func printNodeAccounts(accounts []types.QueryNodeAccount, format string) error {
	if strings.EqualFold("format", "json") {
		result, err := json.MarshalIndent(accounts, "", "	")
		if err != nil {
			return fmt.Errorf("fail to marshal result to json: %w", err)
		}
		fmt.Println(result)
	}
	tw := tablewriter.NewWriter(os.Stdout)
	tw.SetRowLine(true)
	tw.SetHeader([]string{
		"pub key",
		"status",
		"ip",
		"version",
		"request to leave",
		"force to leave",
		"leave height",
		"slash points",
		"bond",
		"node address",
		"online",
	})
	for _, na := range accounts {
		tw.Append([]string{
			na.PubKeySet.Secp256k1.String(),
			getStatusForDisplay(na.Status),
			na.IPAddress,
			na.Version.String(),
			strconv.FormatBool(na.RequestedToLeave),
			strconv.FormatBool(na.ForcedToLeave),
			strconv.FormatInt(na.LeaveHeight, 10),
			strconv.FormatInt(na.SlashPoints, 10),
			getHumanReadableAmount(na.Bond),
			na.NodeAddress.String(),
			getOnlineStatus(isOnline(na.IPAddress)),
		})
	}
	tw.Render()
	return nil
}
func getOnlineStatus(input bool) string {
	if input {
		return aurora.Green("YES").String()
	}
	return aurora.Red("NO").String()
}
func isOnline(ip string) bool {
	url := fmt.Sprintf("http://%s:6040/p2pid", ip)
	client := http.Client{}
	result, err := client.Get(url)
	if err != nil {
		return false
	}
	if result.StatusCode == http.StatusOK {
		return true
	}
	return false
}
func getHumanReadableAmount(input cosmos.Uint) string {
	return strconv.FormatFloat(float64(input.Uint64())/float64(common.One), 'f', -1, 64)
}
func getStatusForDisplay(status types.NodeStatus) string {
	switch status {
	case thorchain.NodeDisabled:
		return aurora.Red(status.String()).String()
	case thorchain.NodeActive:
		return aurora.Green(status.String()).String()
	case thorchain.NodeStandby, thorchain.NodeReady:
		return status.String()
	}
	return status.String()
}
func filterNodeAccountsByFilters(nodeAccounts []types.QueryNodeAccount, filters []filter) []types.QueryNodeAccount {
	var result []types.QueryNodeAccount
	for _, na := range nodeAccounts {
		passFilter := true
		for _, f := range filters {
			if !f(na) {
				passFilter = false
				break
			}
		}
		if passFilter {
			result = append(result, na)
		}
	}
	return result
}
func filterNodeAccountsByStatus(nodeAccounts []types.QueryNodeAccount, multipleStatuses string) []types.QueryNodeAccount {
	var result []types.QueryNodeAccount
	statuses := strings.Split(multipleStatuses, ",")
	for _, s := range statuses {
		status := types.GetNodeStatus(s)
		for _, na := range nodeAccounts {
			if na.Status == status {
				result = append(result, na)
			}
		}
	}
	return result
}
func getNodeAccounts(serverIP string, port int) ([]types.QueryNodeAccount, error) {
	buf, err := getFromThorchain(serverIP, port, "nodeaccounts")
	if err != nil {
		return nil, fmt.Errorf("fail to get node accounts from thorchain")
	}
	var nodeAccounts []types.QueryNodeAccount
	if err := codec.Cdc.UnmarshalJSON(buf, &nodeAccounts); err != nil {
		return nil, fmt.Errorf("fail to unmarshal node accounts: %w", err)
	}
	return nodeAccounts, nil
}
