package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"

	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/logrusorgru/aurora/v3"
	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/x/thorchain/types"
)

func getTxVoterCmd() *cli.Command {
	return &cli.Command{
		Name:        "tx",
		Usage:       "this command is used to retrieve a tx voter from thorchain ",
		UsageText:   "",
		Description: "",
		ArgsUsage:   "",
		Category:    "tx",
		Action:      getTxVoterAction,
	}
}
func getTxVoterAction(ctx *cli.Context) error {
	serverIP := ctx.String("server")
	if serverIP == "" {
		return errors.New("no server ip")
	}
	port := ctx.Int("port")
	if port == 0 {
		port = 1317
	}
	asgard, err := getAsgard(serverIP, port)
	if err != nil {
		return fmt.Errorf("fail to get asgard from thornode: %w", err)
	}
	tw := tablewriter.NewWriter(os.Stdout)
	tw.SetHeader([]string{
		"asgard pub key",
		"pub key",
		"address",
		"signed",
	})

	for _, item := range ctx.Args().Slice() {
		voter, err := getTxVoter(serverIP, port, item)
		if err != nil {
			return fmt.Errorf("fail to get tx voter from thornode: %w", err)
		}

		for _, v := range asgard {
			for _, m := range v.Membership {
				addr, err := m.GetThorAddress()
				if err != nil {
					return fmt.Errorf("fail to get thor address for (%s): %w", m.String(), err)
				}
				signed := false
				for _, signer := range voter.Txs[0].Signers {
					if signer.Equals(addr) {
						signed = true
						break
					}
				}
				strSigned := strconv.FormatBool(signed)
				if signed {
					strSigned = aurora.Green(strSigned).String()
				} else {
					strSigned = aurora.Red(strSigned).String()
				}

				tw.Append([]string{
					v.PubKey.String(),
					m.String(),
					addr.String(),
					strSigned,
				})
			}
		}
	}
	tw.Render()
	return nil
}

func getTxVoter(serverIP string, port int, txID string) (types.ObservedTxVoter, error) {
	buf, err := getFromThorchain(serverIP, port, fmt.Sprintf("/tx/%s/voter", txID))
	if err != nil {
		return types.ObservedTxVoter{}, fmt.Errorf("fail to get  tx voter from thorchain: %w", err)
	}
	var voter types.ObservedTxVoter
	if err := codec.Cdc.UnmarshalJSON(buf, &voter); err != nil {
		return types.ObservedTxVoter{}, fmt.Errorf("fail to unmarshal tx voter:%w", err)
	}
	return voter, nil
}
