package main

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/btcjson"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcd/chaincfg/chainhash"
	"github.com/btcsuite/btcd/rpcclient"
	"github.com/btcsuite/btcd/wire"
	"github.com/btcsuite/btcutil"
	"github.com/olekukonko/tablewriter"
	"github.com/tendermint/tendermint/crypto/secp256k1"
	tos "github.com/tendermint/tendermint/libs/os"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/txscript"
)

var (
	btcdHost           string
	btcRpcUserName     string
	btcRpcUserPassword string
	btcDisableTLS      bool
	btcUseHTTPPostMode bool
)

func getBitcoinCmd() *cli.Command {
	return &cli.Command{
		Name:        "bitcoin",
		Aliases:     []string{"b"},
		Usage:       "this command is used to communicate with a bitcoind node",
		UsageText:   "",
		Description: "",
		ArgsUsage:   "",
		Category:    "bitcoin",
		Action:      bitcoinCmdAction,
		Subcommands: []*cli.Command{
			&cli.Command{
				Name:        "chaininfo",
				Aliases:     nil,
				Usage:       "get block chain info",
				UsageText:   "",
				Description: "get block chain info",
				ArgsUsage:   "",
				Category:    "",
				Action:      getBitcoinChainInforAction,
			},
			&cli.Command{
				Name:        "blockstats",
				Aliases:     nil,
				Usage:       "get block stats info",
				UsageText:   "",
				Description: "get block stats infor",
				ArgsUsage:   "",
				Category:    "",
				Action:      getBlockStatsAction,
			},
			&cli.Command{
				Name:     "get",
				Aliases:  nil,
				Usage:    "get a tx by hash",
				Category: "tx",
				Action:   getTransactionAction,
			},
			&cli.Command{
				Name:     "getblock",
				Aliases:  nil,
				Usage:    "get a block",
				Category: "tx",
				Action:   getBlockAction,
			},
			&cli.Command{
				Name:        "send",
				Aliases:     nil,
				Usage:       "transfer Bitcoin to another address",
				Description: "send Bitcoin from an address to another",
				ArgsUsage:   "[hex_encoded_from_private_key] [to] [amount] [memo]",
				Category:    "wallet",
				Action:      sendBitcoinAction,
			},
			&cli.Command{
				Name:        "import",
				Usage:       "hex encoded private keys",
				UsageText:   "you can use this command to import a private key as a wallet",
				Description: "",
				ArgsUsage:   "hex encoded private keys",
				Category:    "wallet",
				Action:      importWalletAction,
				Hidden:      false,
			},
			&cli.Command{
				Name:      "watch",
				Aliases:   nil,
				Usage:     "add address to watch list",
				UsageText: "you can use this command to add addresses to the wallet's watch list",
				ArgsUsage: "",
				Category:  "wallet",
				Action:    addWatchAction,
				Hidden:    false,
			},
			&cli.Command{
				Name:        "list",
				Usage:       "list unspent utxo",
				UsageText:   "list unspent utxo",
				Description: "",
				Category:    "wallet",
				Action:      listUnSpentUTXO,
				Hidden:      false,
				ArgsUsage:   "list address",
			},
			&cli.Command{
				Name:         "create",
				Aliases:      nil,
				Usage:        "create a wallet",
				UsageText:    "create a wallet",
				Description:  "",
				ArgsUsage:    "",
				Category:     "wallet",
				Action:       createWalletAction,
				OnUsageError: nil,
				Subcommands:  nil,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "name",
						Aliases:     nil,
						Usage:       "wallet name",
						Required:    true,
						Hidden:      false,
						TakesFile:   false,
						Value:       "default",
						DefaultText: "default wallet",
						Destination: nil,
						HasBeenSet:  true,
					},
				},
				SkipFlagParsing: false,
				Hidden:          false,
			},
			&cli.Command{
				Name:      "getblockdetail",
				Usage:     "get transactions of a block",
				UsageText: "get all transactions of a block",
				Action:    getBlockDetailAction,
			},
			&cli.Command{
				Name:      "coinbase",
				Usage:     "get coinbase value of a block",
				UsageText: "get coinbase value of a block",
				Action:    getCoinbaseValueAction,
			},
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "host",
				Usage:       "bitcoind node rpc host",
				Required:    true,
				Hidden:      false,
				Value:       "127.0.0.1:18443",
				Destination: &btcdHost,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "username",
				Usage:       "username used to talk to bitcoind",
				Required:    true,
				Hidden:      false,
				Value:       "thorchain",
				Destination: &btcRpcUserName,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "password",
				Usage:       "password used to talk to bitcoind",
				Required:    true,
				Hidden:      false,
				Value:       "password",
				Destination: &btcRpcUserPassword,
				HasBeenSet:  true,
			},
			&cli.BoolFlag{
				Name:        "disable-tls",
				Usage:       "disable TLS",
				Required:    true,
				Hidden:      false,
				Value:       true,
				DefaultText: "disable TLS mode",
				Destination: &btcDisableTLS,
				HasBeenSet:  true,
			},
			&cli.BoolFlag{
				Name:        "post-mode",
				Usage:       "use HTTP Post Mode",
				Required:    true,
				Hidden:      false,
				Value:       true,
				DefaultText: "",
				Destination: &btcUseHTTPPostMode,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "network",
				Usage:       "bitcoin network, [mainnet,testnet,regtest]",
				Required:    true,
				Hidden:      false,
				Value:       "regtest",
				DefaultText: "",
				Destination: nil,
				HasBeenSet:  true,
			},
		},
	}
}
func getBitcoinClient() *rpcclient.Client {
	client, err := rpcclient.New(&rpcclient.ConnConfig{
		Host:         btcdHost,
		User:         btcRpcUserName,
		Pass:         btcRpcUserPassword,
		DisableTLS:   btcDisableTLS,
		HTTPPostMode: btcUseHTTPPostMode,
	}, nil)
	if err != nil {
		panic(err)
	}
	return client
}

func getTransactionAction(ctx *cli.Context) error {
	if ctx.NArg() == 0 {
		return errors.New("please provide transaction hash")
	}
	client := getBitcoinClient()
	for _, item := range ctx.Args().Slice() {
		hash, err := chainhash.NewHashFromStr(item)
		if err != nil {
			return fmt.Errorf("fail to parse hash(%s): %w", item, err)
		}
		result, err := client.GetRawTransactionVerbose(hash)
		if err != nil {
			return fmt.Errorf("fail to get transaction with hash(%s) : %w", item, err)
		}
		buf, err := json.MarshalIndent(result, "", "	")
		if err != nil {
			return fmt.Errorf("fail to marshal result to json: %w", err)
		}
		fmt.Println(string(buf))
	}
	return nil
}

func addWatchAction(ctx *cli.Context) error {
	if ctx.NArg() == 0 {
		return errors.New("please provide address")
	}
	client := getBitcoinClient()
	for _, item := range ctx.Args().Slice() {
		addr, err := btcutil.DecodeAddress(item, getBitcoinNetworkConfig(ctx))
		if err != nil {
			panic(err)
		}
		if err := client.ImportAddressRescan(addr.String(), item, true); err != nil {
			return fmt.Errorf("fail to import address: %w", err)
		}
	}
	return nil
}
func getBitcoinNetworkConfig(ctx *cli.Context) *chaincfg.Params {
	net := ctx.String("network")
	switch net {
	case chaincfg.RegressionNetParams.Name:
		return &chaincfg.RegressionNetParams
	case chaincfg.MainNetParams.Name:
		return &chaincfg.MainNetParams
	case chaincfg.TestNet3Params.Name:
		return &chaincfg.TestNet3Params
	}
	return nil
}
func listUnSpentUTXO(ctx *cli.Context) error {
	client := getBitcoinClient()
	if ctx.NArg() == 0 {
		return errors.New("please provide address")
	}
	addr, err := btcutil.DecodeAddress(ctx.Args().First(), getBitcoinNetworkConfig(ctx))
	if err != nil {
		panic(err)
	}
	result, err := client.ListUnspentMinMaxAddresses(0, 99999, []btcutil.Address{
		addr,
	})
	if err != nil {
		return fmt.Errorf("fail to list account: %w", err)
	}
	sort.SliceStable(result, func(i, j int) bool {
		return result[i].Confirmations > result[j].Confirmations
	})
	format := ctx.String("format")
	if strings.EqualFold(format, "json") {
		result, err := json.MarshalIndent(result, "", "	")
		if err != nil {
			return fmt.Errorf("fail to marshal result to json: %w", err)
		}
		fmt.Println(string(result))
		return nil
	}
	tw := tablewriter.NewWriter(os.Stdout)
	tw.SetHeader([]string{
		"Address", "txid", "amount", "vout", "account", "confirmation", "pubkey",
	})
	for _, item := range result {
		tw.Append([]string{
			item.Address,
			item.TxID,
			strconv.FormatFloat(item.Amount, 'f', 2, 64),
			fmt.Sprintf("%d", item.Vout),
			item.Account,
			strconv.FormatInt(item.Confirmations, 10),
			item.ScriptPubKey,
		})
	}
	tw.Render()
	return nil
}

func importWalletAction(ctx *cli.Context) error {
	client := getBitcoinClient()
	if ctx.NArg() == 0 {
		return errors.New("please provide hex encode private key")
	}

	for _, item := range ctx.Args().Slice() {
		buf, err := hex.DecodeString(item)
		if err != nil {
			return fmt.Errorf("fail to decode (%s) as hex string: %w", item, err)
		}
		pkey, _ := btcec.PrivKeyFromBytes(btcec.S256(), buf)
		w, err := btcutil.NewWIF(pkey, getBitcoinNetworkConfig(ctx), false)
		if err != nil {
			return fmt.Errorf("fail to create WIF: %w", err)
		}
		addr, err := getAddressFromPrivateKey(ctx, pkey)
		if err != nil {
			return fmt.Errorf("fail to get address from private key: %w", err)
		}
		if err := client.ImportPrivKey(w); err != nil {
			return fmt.Errorf("fail to import private key: %w", err)
		}
		fmt.Printf("%s -  account address is : %s\n", item, addr)
	}

	return nil
}

func createWalletAction(ctx *cli.Context) error {
	name := ctx.String("name")
	ext := filepath.Ext(name)
	if ext == "" {
		name = name + ".dat"
	}
	if tos.FileExists(name) {
		buf, err := ioutil.ReadFile(name)
		if err != nil {
			return fmt.Errorf("fail to open file (%s): %w", name, err)
		}
		decodeBuf, err := hex.DecodeString(string(buf))
		if err != nil {
			return fmt.Errorf("fail to decode hex string(%s): %w", string(buf), err)
		}
		priKey, _ := btcec.PrivKeyFromBytes(btcec.S256(), decodeBuf)
		addr, err := getAddressFromPrivateKey(ctx, priKey)
		if err != nil {
			return fmt.Errorf("fail to get address from private key: %w", err)
		}
		fmt.Println("wallet already exist")
		fmt.Println("your address is:", addr)
		return nil
	}
	privateKey, err := btcec.NewPrivateKey(btcec.S256())
	if err != nil {
		return fmt.Errorf("fail to create a new private key: %w", err)
	}
	buf := privateKey.Serialize()
	pkey, _ := btcec.PrivKeyFromBytes(btcec.S256(), buf)
	ioutil.WriteFile(name, []byte(hex.EncodeToString(pkey.Serialize())), 0644)
	addr, err := getAddressFromPrivateKey(ctx, privateKey)
	if err != nil {
		return fmt.Errorf("fail to get address from private key: %w", err)
	}
	fmt.Println("your address is:", addr)
	return nil
}
func getAddressFromPrivateKey(ctx *cli.Context, priKey *btcec.PrivateKey) (string, error) {
	pubKeyBuf := priKey.PubKey().SerializeCompressed()
	var secpPubKey secp256k1.PubKeySecp256k1
	copy(secpPubKey[:], pubKeyBuf)

	addr, err := btcutil.NewAddressWitnessPubKeyHash(secpPubKey.Address().Bytes(), getBitcoinNetworkConfig(ctx))
	if err != nil {
		return "", fmt.Errorf("fail to create address: %w", err)
	}
	//
	// oldAddr, err := btcutil.NewAddressPubKeyHash(secpPubKey.Address().Bytes(), getBitcoinNetworkConfig(ctx))
	// if err != nil {
	//	return "", fmt.Errorf("fail to create old address: %w", err)
	// }
	//
	// fmt.Println("oldAddr:", oldAddr.EncodeAddress())
	return addr.EncodeAddress(), nil
}

func sendBitcoinAction(ctx *cli.Context) error {
	if ctx.NArg() < 4 {
		return fmt.Errorf("from_private_key to_address amount memo")
	}
	client := getBitcoinClient()
	from := ctx.Args().Get(0)
	to := ctx.Args().Get(1)
	fromPriKey, err := hex.DecodeString(from)
	if err != nil {
		return fmt.Errorf("fail to decode from private key: %w", err)
	}
	pkey, _ := btcec.PrivKeyFromBytes(btcec.S256(), fromPriKey)
	fromAddr, err := getAddressFromPrivateKey(ctx, pkey)
	if err != nil {
		return fmt.Errorf("fail to get address from private key: %w", err)
	}
	fromAddress, err := btcutil.DecodeAddress(fromAddr, getBitcoinNetworkConfig(ctx))
	if err != nil {
		return fmt.Errorf("fail to decode from address(%s): %w", from, err)
	}
	payToSource, err := txscript.PayToAddrScript(fromAddress)
	if err != nil {
		return fmt.Errorf("fail to get pay to address script: %w", err)
	}

	toAddr, err := btcutil.DecodeAddress(to, getBitcoinNetworkConfig(ctx))
	if err != nil {
		return fmt.Errorf("fail to decode address(%s): %w", to, err)
	}
	amt := ctx.Args().Get(2)
	fAmt, err := strconv.ParseFloat(amt, 64)
	if err != nil {
		return fmt.Errorf("%s is invalid amount: %w", amt, err)
	}
	var memo string
	if ctx.NArg() >= 4 {
		memo = ctx.Args().Get(3)
	}
	gas := float64(40000) / float64(common.One)
	// add a little bit gas
	utxoes, err := getUTXO(fromAddress, fAmt+gas)
	if err != nil {
		return fmt.Errorf("fail to find utxo for address(%s): %w", from, err)
	}
	if len(utxoes) == 0 {
		return errors.New("no unspent utxo")
	}
	btcAmt, err := btcutil.NewAmount(fAmt)
	if err != nil {
		return fmt.Errorf("fail to parse amount: %w", err)
	}
	redeemTx := wire.NewMsgTx(wire.TxVersion)
	totalAmt := float64(0)
	individualAmounts := make(map[chainhash.Hash]btcutil.Amount, len(utxoes))
	for _, item := range utxoes {
		txID, err := chainhash.NewHashFromStr(item.TxID)
		if err != nil {
			return fmt.Errorf("fail to create hash from string: %w", err)
		}
		// double check that the utxo is still valid
		outputPoint := wire.NewOutPoint(txID, item.Vout)
		sourceTxIn := wire.NewTxIn(outputPoint, nil, nil)
		redeemTx.AddTxIn(sourceTxIn)
		totalAmt += item.Amount
		amt, err := btcutil.NewAmount(item.Amount)
		if err != nil {
			return fmt.Errorf("fail to parse amount(%f): %w", item.Amount, err)
		}
		individualAmounts[*txID] = amt
	}
	buf, err := txscript.PayToAddrScript(toAddr)
	if err != nil {
		return fmt.Errorf("fail to get pay to address script: %w", err)
	}

	total, err := btcutil.NewAmount(totalAmt)
	if err != nil {
		return fmt.Errorf("fail to parse total amount(%f),err: %w", totalAmt, err)
	}

	// pay to customer
	redeemTxOut := wire.NewTxOut(int64(btcAmt.ToUnit(btcutil.AmountSatoshi)), buf)
	redeemTx.AddTxOut(redeemTxOut)

	// balance to ourselves
	// add output to pay the balance back ourselves
	balance := int64(total) - redeemTxOut.Value - 40000
	if balance < 0 {
		return errors.New("not enough balance to pay customer")
	}
	if balance > 0 {
		redeemTx.AddTxOut(wire.NewTxOut(balance, payToSource))
	}

	if len(memo) != 0 {
		// memo
		nullDataScript, err := txscript.NullDataScript([]byte(memo))
		if err != nil {
			return fmt.Errorf("fail to generate null data script: %w", err)
		}
		redeemTx.AddTxOut(wire.NewTxOut(0, nullDataScript))
	}
	// sort inputs and outputs
	// txsort.InPlaceSort(redeemTx)

	for idx, txIn := range redeemTx.TxIn {
		sigHashes := txscript.NewTxSigHashes(redeemTx)
		sig := txscript.NewPrivateKeySignable(pkey)
		outputAmount := int64(individualAmounts[txIn.PreviousOutPoint.Hash])
		witness, err := txscript.WitnessSignature(redeemTx, sigHashes, idx, outputAmount, payToSource, txscript.SigHashAll, sig, true)
		if err != nil {
			return fmt.Errorf("fail to get witness: %w", err)
		}

		redeemTx.TxIn[idx].Witness = witness
		flag := txscript.StandardVerifyFlags
		engine, err := txscript.NewEngine(payToSource, redeemTx, idx, flag, nil, nil, outputAmount)
		if err != nil {
			return fmt.Errorf("fail to create engine: %w", err)
		}
		if err := engine.Execute(); err != nil {
			return fmt.Errorf("fail to execute the script: %w", err)
		}
	}

	h, err := client.SendRawTransaction(redeemTx, true)
	if err != nil {
		return fmt.Errorf("fail to broadcast raw transaction: %w", err)
	}
	fmt.Println("hash:", h.String())
	return nil
}

func getUTXO(from btcutil.Address, amount float64) ([]btcjson.ListUnspentResult, error) {
	client := getBitcoinClient()
	result, err := client.ListUnspentMinMaxAddresses(0, 99999, []btcutil.Address{
		from,
	})
	if err != nil {
		return nil, fmt.Errorf("fail to get unspent utox: %w", err)
	}
	var utxo []btcjson.ListUnspentResult
	totalAmount := amount
	for _, item := range result {
		if item.Amount == 0 {
			continue
		}
		utxo = append(utxo, item)
		if item.Amount < totalAmount {
			totalAmount -= item.Amount
		} else {
			break
		}
	}
	return utxo, nil
}

func getBlockAction(ctx *cli.Context) error {
	client := getBitcoinClient()
	strHeight := ctx.Args().First()
	height, err := strconv.ParseInt(strHeight, 10, 64)
	if err != nil {
		return fmt.Errorf("convert block height(%s) to int: %w", strHeight, err)
	}
	hash, err := client.GetBlockHash(height)
	if err != nil {
		return fmt.Errorf("fail to get block hash: %w", err)
	}
	result, err := client.GetBlockVerboseTx(hash)
	if err != nil {
		return fmt.Errorf("fail to get block verbose tx: %w", err)
	}
	buf, err := json.MarshalIndent(result, "", "	")
	if err != nil {
		return fmt.Errorf("fail to marshal block result to json: %w", err)
	}
	fmt.Println(string(buf))
	return nil
}

func getBitcoinChainInforAction(ctx *cli.Context) error {
	client := getBitcoinClient()
	blockResultInfo, err := client.GetBlockChainInfo()
	if err != nil {
		return fmt.Errorf("fail to get block chain info: %w", err)
	}
	format := ctx.String("format")
	if strings.EqualFold(format, "json") {
		buf, err := json.MarshalIndent(blockResultInfo, "", "	")
		if err != nil {
			return fmt.Errorf("fail to marshal block result info to json: %w", err)
		}
		fmt.Println(string(buf))
		return nil
	}
	fmt.Println("chain:", blockResultInfo.Chain)
	fmt.Println("best block hash:", blockResultInfo.BestBlockHash)
	fmt.Println("blocks:", blockResultInfo.Blocks)
	fmt.Println("chain work:", blockResultInfo.ChainWork)
	fmt.Println("difficulty:", blockResultInfo.Difficulty)
	fmt.Println("headers", blockResultInfo.Headers)
	fmt.Println("median time:", blockResultInfo.MedianTime)
	fmt.Println("pruned:", blockResultInfo.Pruned)
	fmt.Println("prune height:", blockResultInfo.PruneHeight)
	fmt.Println("verification progress", blockResultInfo.VerificationProgress)
	return nil
}

func bitcoinCmdAction(ctx *cli.Context) error {
	client := getBitcoinClient()
	blockResultInfo, err := client.GetBlockChainInfo()
	if err != nil {
		return fmt.Errorf("fail to get block chain info: %w", err)
	}
	fmt.Println(blockResultInfo)
	return nil
}

// get block detail information
func getBlockDetailAction(ctx *cli.Context) error {
	if ctx.NArg() == 0 {
		wg := sync.WaitGroup{}
		wg.Add(1)
		client := getBitcoinClient()
		go func() {
			defer wg.Done()
			c := make(chan os.Signal, 1)
			signal.Notify(c, os.Interrupt)
			var preHeight int64
			for {
				select {
				case <-c:
					return
				default:
					height, err := client.GetBlockCount()
					if err != nil {
						fmt.Println("fail to get best block:", err)
						continue
					}
					if height != preHeight {
						h, err := client.GetBlockHash(height)
						if err != nil {
							fmt.Println("fail to get block hash for height:", height, err)
							continue
						}
						if err := getBlockDetail(h.String()); err != nil {
							fmt.Println(fmt.Sprintf("fail to get block detail for(%s): %s", h, err))
						}
						preHeight = height
					} else {
						time.Sleep(time.Second * 30)
					}
				}
			}
		}()
		wg.Wait()
	} else {
		for _, hash := range ctx.Args().Slice() {
			if err := getBlockDetail(hash); err != nil {
				fmt.Println(fmt.Sprintf("fail to get block detail for(%s): %s", hash, err))
			}
		}
	}
	return nil
}

func getBlockDetail(hash string) error {
	client := getBitcoinClient()
	h, err := chainhash.NewHashFromStr(hash)
	if err != nil {
		return fmt.Errorf("fail to parse hash(%s):%w", hash, err)
	}
	blockStats, err := client.GetBlockStats(h, nil)
	if err != nil {
		return fmt.Errorf("fail to get block stats with hash(%s) : %w", hash, err)
	}
	totalFee := blockStats.AverageFee * blockStats.Txs
	amt := btcutil.Amount(totalFee + blockStats.Subsidy)
	result, err := client.GetBlockVerboseTx(h)
	if err != nil {
		return fmt.Errorf("fail to get block result: %w", err)
	}

	totalTxValue := getTotalTransactionValue(result.Tx)
	if totalTxValue == 0 {
		return nil
	}
	amtTotalValue, err := btcutil.NewAmount(totalTxValue)
	if err != nil {
		return fmt.Errorf("fail to create amount: %w", err)
	}

	fmt.Println("block hash:", hash)
	fmt.Println("median fee:", blockStats.MedianFee, "median size:", blockStats.MedianTxSize)
	fmt.Println("Total transaction value:", strconv.FormatFloat(amtTotalValue.ToUnit(btcutil.AmountBTC), 'f', 2, 64))
	fmt.Println("Total fee and subsidy:", strconv.FormatFloat(amt.ToUnit(btcutil.AmountBTC), 'f', 2, 64))
	fmt.Println("delay:", int(amtTotalValue.ToUnit(btcutil.AmountSatoshi)*2/amt.ToUnit(btcutil.AmountSatoshi)))
	return err
}

func getTotalTransactionValue(source []btcjson.TxRawResult) float64 {
	var totalValue float64
	for _, item := range source {
		txInput := getTransactionValue(item)
		totalValue += txInput

	}
	return totalValue
}

func getTransactionValue(source btcjson.TxRawResult) float64 {
	var totalOutput float64
	for _, item := range source.Vin {
		if item.IsCoinBase() {
			return totalOutput
		}
	}
	for _, item := range source.Vout {
		totalOutput += item.Value
	}
	return totalOutput
}

func getBlockStatsAction(ctx *cli.Context) error {
	client := getBitcoinClient()
	for _, item := range ctx.Args().Slice() {
		blockHeight, err := strconv.ParseInt(item, 10, 64)
		if err != nil {
			return fmt.Errorf("fail to parse block height(%s): %w", item, err)
		}
		result, err := client.GetBlockStats(blockHeight, nil)
		if err != nil {
			return fmt.Errorf("fail to get block stats: %w", err)
		}
		jsonResult, err := json.MarshalIndent(result, "", "	")
		if err != nil {
			return fmt.Errorf("fail to marshal result to json: %w", err)
		}
		fmt.Println("block:", item)
		fmt.Println(string(jsonResult))
	}
	return nil
}

func getCoinbaseValueAction(ctx *cli.Context) error {
	client := getBitcoinClient()
	for _, item := range ctx.Args().Slice() {
		blockHeight, err := strconv.ParseInt(item, 10, 64)
		if err != nil {
			return fmt.Errorf("fail to parse block height(%s): %w", item, err)
		}
		hash, err := client.GetBlockHash(blockHeight)
		if err != nil {
			return fmt.Errorf("fail to get block hash:%w", err)
		}
		fmt.Println("block height:", blockHeight, " hash:", hash.String())
		result, err := client.GetBlockVerboseTx(hash)
		if err != nil {
			return fmt.Errorf("fail to get block verbose tx: %w", err)
		}
		fmt.Println("txs:", len(result.Tx))
		for _, tx := range result.Tx {
			if len(tx.Vin) == 1 && tx.Vin[0].IsCoinBase() {
				total := float64(0)
				for _, opt := range tx.Vout {
					total += opt.Value
				}
				fmt.Println("total coinbase value is:", total)
				break
			}
		}
	}
	return nil
}
