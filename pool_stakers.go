package main

import (
	"errors"
	"fmt"

	"github.com/cosmos/cosmos-sdk/codec"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/common/cosmos"
	"gitlab.com/thorchain/thornode/x/thorchain/types"
)

func getPoolStakers() *cli.Command {
	return &cli.Command{
		Name:        "stakers",
		Usage:       "this command is used to get all stakers of a given pool",
		UsageText:   "get all the stakers from thorchain ",
		Description: "",
		ArgsUsage:   "",
		Category:    "staker",
		Action:      getStakersAction,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:       "pool",
				Usage:      "the asset name of the pool",
				Required:   true,
				Hidden:     false,
				Value:      "",
				HasBeenSet: false,
			},
		},
	}
}

func getPool(ctx *cli.Context, asset string) (types.Pool, error) {
	serverIP := ctx.String("server")
	if serverIP == "" {
		return types.Pool{}, errors.New("no server ip")
	}
	port := ctx.Int("port")
	if port == 0 {
		port = 1317
	}
	buf, err := getFromThorchain(serverIP, port, fmt.Sprintf("pool/%s", asset))
	if err != nil {
		return types.Pool{}, fmt.Errorf("fail to get pool from thorchain")
	}
	var pool types.Pool
	if err := codec.Cdc.UnmarshalJSON(buf, &pool); err != nil {
		return types.Pool{}, fmt.Errorf("fail to unmarshal pool: %w", err)
	}
	return pool, nil
}

func getStakersAction(ctx *cli.Context) error {
	serverIP := ctx.String("server")
	if serverIP == "" {
		return errors.New("no server ip")
	}
	port := ctx.Int("port")
	if port == 0 {
		port = 1317
	}

	pool := ctx.String("pool")
	buf, err := getFromThorchain(serverIP, port, fmt.Sprintf("pool/%s/stakers", pool))
	if err != nil {
		return fmt.Errorf("fail to get stakers from pool(%s):%w", pool, err)
	}
	var result []types.Staker
	if err := codec.Cdc.UnmarshalJSON(buf, &result); err != nil {
		return fmt.Errorf("fail to unmarshal stakers: %w", err)
	}
	p, err := getPool(ctx, pool)
	if err != nil {
		return fmt.Errorf("fail to get pool from thorchain: %w", err)
	}
	totalUnits := uint64(0)
	for _, item := range result {
		totalUnits += item.Units.Uint64()
	}
	fmt.Println("total unit for ", pool, " is:", totalUnits)
	fmt.Println("total unit from thorchain is", p.PoolUnits.String())
	runeStake := cosmos.NewUint(87300000000)
	assetStake := cosmos.NewUint(23300000000)
	for _, item := range result {
		r, a, _, err := calculateUnstake(p.PoolUnits, p.BalanceRune, p.BalanceAsset, item.Units, cosmos.NewUint(10000))
		if err != nil {
			panic(err)
		}
		fmt.Printf("runeStake:%s, asset Stake:%s, rune: %s, asset: %s", runeStake.QuoUint64(common.One), assetStake.QuoUint64(common.One), r, a.QuoUint64(common.One))
		if r.LT(runeStake) && a.LT(assetStake) {
			fmt.Println(" FAIL")
		} else {
			fmt.Println(" OK")
		}
	}
	return nil
}
func calculateUnstake(poolUnits, poolRune, poolAsset, stakerUnits, withdrawBasisPoints cosmos.Uint) (cosmos.Uint, cosmos.Uint, cosmos.Uint, error) {
	if poolUnits.IsZero() {
		return cosmos.ZeroUint(), cosmos.ZeroUint(), cosmos.ZeroUint(), errors.New("poolUnits can't be zero")
	}
	if poolRune.IsZero() {
		return cosmos.ZeroUint(), cosmos.ZeroUint(), cosmos.ZeroUint(), errors.New("pool rune balance can't be zero")
	}
	if poolAsset.IsZero() {
		return cosmos.ZeroUint(), cosmos.ZeroUint(), cosmos.ZeroUint(), errors.New("pool asset balance can't be zero")
	}
	if stakerUnits.IsZero() {
		return cosmos.ZeroUint(), cosmos.ZeroUint(), cosmos.ZeroUint(), errors.New("staker unit can't be zero")
	}
	if withdrawBasisPoints.GT(cosmos.NewUint(10000)) {
		return cosmos.ZeroUint(), cosmos.ZeroUint(), cosmos.ZeroUint(), fmt.Errorf("withdraw basis point %s is not valid", withdrawBasisPoints.String())
	}

	unitsToClaim := common.GetShare(withdrawBasisPoints, cosmos.NewUint(10000), stakerUnits)
	withdrawRune := common.GetShare(unitsToClaim, poolUnits, poolRune)
	withdrawAsset := common.GetShare(unitsToClaim, poolUnits, poolAsset)
	unitAfter := common.SafeSub(stakerUnits, unitsToClaim)
	return withdrawRune, withdrawAsset, unitAfter, nil
}
