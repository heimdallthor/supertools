package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/logrusorgru/aurora/v3"
	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/common/cosmos"
	"gitlab.com/thorchain/tss/go-tss/conversion"
)

func setupConfig(env common.ChainNetwork) {
	switch env {
	case common.TestNet:
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("tthor", "tthorpub")
		config.SetBech32PrefixForValidator("tthorv", "tthorvpub")
		config.SetBech32PrefixForConsensusNode("tthorc", "tthorcpub")
	case common.MainNet:
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("thor", "thorpub")
		config.SetBech32PrefixForValidator("thorv", "thorvpub")
		config.SetBech32PrefixForConsensusNode("thorc", "thorcpub")
	}
}

func main() {
	app := &cli.App{
		Name:  "convert keys",
		Usage: "a tool convert from pub key to different kind of other keys (P2PID, thor address, BNB address etc)",
		Commands: []*cli.Command{
			&cli.Command{
				Name: "address",
				Aliases: []string{
					"addr",
				},
				Usage:     "given a public key , print out all addresses",
				ArgsUsage: "[public keys, multiple public keys separate by space]",
				Category:  "address",
				Action:    getAddresses,
			},
			&cli.Command{
				Name:     "p2pid",
				Aliases:  []string{"p2p"},
				Usage:    "given a p2p id , print out all addresses related",
				Category: "address",
				Action:   getAddressFromP2P,
			},
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "server",
				Aliases:  []string{"s"},
				Usage:    "server url or ip address",
				EnvVars:  []string{"SERVER_URL"},
				Value:    "localhost",
				Required: false,
				Hidden:   false,
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
func getAddressFromP2P(ctx *cli.Context) error {
	tw := tablewriter.NewWriter(os.Stdout)
	tw.SetHeader([]string{"NETWORK", "pub key", common.THORChain.String(), common.BTCChain.String(), common.BNBChain.String(), common.ETHChain.String(), "P2P ID"})
	for _, pid := range ctx.Args().Slice() {
		getAddrFromP2P(pid, common.TestNet, tw)
		getAddrFromP2P(pid, common.MainNet, tw)
	}
	tw.SetBorder(true)
	tw.Render()
	return nil
}
func getAddrFromP2P(peerID string, network common.ChainNetwork, tw *tablewriter.Table) error {
	setupConfig(network)
	pubkey, err := conversion.GetPubKeyFromPeerID(peerID)
	if err != nil {
		return fmt.Errorf("fail to get public from peer id: %w", err)
	}
	switch network {
	case common.TestNet:
		os.Setenv("NET", "testnet")
	case common.MainNet:
		os.Setenv("NET", "mainnet")
	}
	if err := getAddr(pubkey, network, tw); err != nil {
		return err
	}
	return nil
}
func getAddresses(ctx *cli.Context) error {
	tw := tablewriter.NewWriter(os.Stdout)
	tw.SetHeader([]string{"NETWORK", "pub key", common.THORChain.String(), common.BTCChain.String(), common.BNBChain.String(), common.ETHChain.String(), "P2P ID"})
	for _, a := range ctx.Args().Slice() {
		fmt.Println("public key: ", a)
		if strings.HasPrefix(a, "tthor") {
			os.Setenv("NET", "testnet")
			getAddr(a, common.TestNet, tw)
		} else if strings.HasPrefix(a, "thor") {
			os.Setenv("NET", "mainnet")
			getAddr(a, common.MainNet, tw)
		}
	}
	tw.Render()
	return nil
}

func getAddr(key string, env common.ChainNetwork, tw *tablewriter.Table) error {
	setupConfig(env)
	pkey, err := common.NewPubKey(key)
	if err != nil {
		return fmt.Errorf("fail to parse public key: %w", err)
	}
	chains := common.Chains{
		common.THORChain,
		common.BTCChain,
		common.BNBChain,
		common.ETHChain,
	}
	content := []string{
		key,
	}
	switch env {
	case common.MainNet:
		content = append([]string{aurora.Red("MAINNET").String()}, content...)
	case common.TestNet:
		content = append([]string{aurora.Green("TESTNET").String()}, content...)
	}

	for _, c := range chains {
		addr, err := pkey.GetAddress(c)
		if err != nil {
			return fmt.Errorf("fail to get address for %s: %w", c, err)
		}
		content = append(content, addr.String())
	}

	peerID, err := conversion.GetPeerIDFromPubKey(key)
	if err != nil {
		return fmt.Errorf("fail to get peer id: %w", err)
	}
	content = append(content, peerID.String())
	tw.Append(content)
	return nil
}
