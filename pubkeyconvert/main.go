package main

import (
	"fmt"

	cosmos "github.com/cosmos/cosmos-sdk/types"
)

func main() {
	var (
		Bech32PrefixAccAddr  = "thor"
		Bech32PrefixAccPub   = "thorpub"
		Bech32PrefixValAddr  = "thorv"
		Bech32PrefixValPub   = "thorvpub"
		Bech32PrefixConsAddr = "thorc"
		Bech32PrefixConsPub  = "thorcpub"
	)
	config := cosmos.GetConfig()
	config.SetBech32PrefixForAccount(Bech32PrefixAccAddr, Bech32PrefixAccPub)
	config.SetBech32PrefixForValidator(Bech32PrefixValAddr, Bech32PrefixValPub)
	config.SetBech32PrefixForConsensusNode(Bech32PrefixConsAddr, Bech32PrefixConsPub)

	pk, err := cosmos.GetPubKeyFromBech32(cosmos.Bech32PubKeyTypeAccPub, "thorpub1addwnpepqggfxj8uyj27drh3lk37mwwq5qcwyl04rwfcvl4vy3v9flgd5xzjydpscyv")
	if err != nil {
		panic(err)
	}
	mainnetAddr := cosmos.AccAddress(pk.Address().Bytes())
	fmt.Println("mainnet addr:", mainnetAddr.String())

	Bech32PrefixAccAddr = "tthor"
	Bech32PrefixAccPub = "tthorpub"
	Bech32PrefixValAddr = "tthorv"
	Bech32PrefixValPub = "tthorvpub"
	Bech32PrefixConsAddr = "tthorc"
	Bech32PrefixConsPub = "tthorcpub"
	config.SetBech32PrefixForAccount(Bech32PrefixAccAddr, Bech32PrefixAccPub)
	config.SetBech32PrefixForValidator(Bech32PrefixValAddr, Bech32PrefixValPub)
	config.SetBech32PrefixForConsensusNode(Bech32PrefixConsAddr, Bech32PrefixConsPub)

	s, err := cosmos.Bech32ifyPubKey(cosmos.Bech32PubKeyTypeAccPub, pk)
	if err != nil {
		panic(err)
	}
	fmt.Println(s)
	testnetAddr := cosmos.AccAddress(pk.Address().Bytes())
	fmt.Println("testnet addr:", testnetAddr.String())

}
