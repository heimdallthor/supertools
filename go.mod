module gitlab.com/thorchain/heimdall/supertools

go 1.15

replace (
	github.com/binance-chain/go-sdk => gitlab.com/thorchain/binance-sdk v1.2.2
	github.com/binance-chain/tss-lib => gitlab.com/thorchain/tss/tss-lib v0.0.0-20200723071108-d21a17ff2b2e
)

require (
	github.com/binance-chain/go-sdk v1.2.3
	github.com/blang/semver v3.5.1+incompatible
	github.com/btcsuite/btcd v0.21.0-beta
	github.com/btcsuite/btcutil v1.0.2
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/cosmos/cosmos-sdk v0.39.0
	github.com/gopherjs/gopherjs v0.0.0-20190430165422-3e4dfb77656c // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/logrusorgru/aurora/v3 v3.0.0
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/olekukonko/tablewriter v0.0.2-0.20190409134802-7e037d187b0c
	github.com/prometheus/common v0.14.0 // indirect
	github.com/prometheus/procfs v0.2.0 // indirect
	github.com/tendermint/tendermint v0.33.6
	github.com/urfave/cli/v2 v2.2.0
	gitlab.com/thorchain/thornode v0.0.0-20200826104256-8424cc60d18f
	gitlab.com/thorchain/tss/go-tss v0.0.0-20200814003838-17010ef8767e
	gitlab.com/thorchain/txscript v0.0.0-20200413023754-8aaf3443d92b
	golang.org/x/crypto v0.0.0-20201002170205-7f63de1d35b0 // indirect
	golang.org/x/sys v0.0.0-20201008064518-c1f3e3309c71 // indirect
	golang.org/x/tools v0.0.0-20201008025239-9df69603baec // indirect
)
