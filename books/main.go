package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	ctypes "github.com/binance-chain/go-sdk/common/types"
	ttypes "github.com/binance-chain/go-sdk/types"
	"github.com/cosmos/cosmos-sdk/codec"
	cosmos "github.com/cosmos/cosmos-sdk/types"
	"github.com/logrusorgru/aurora/v3"
	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli/v2"
	"gitlab.com/thorchain/thornode/common"
	"gitlab.com/thorchain/thornode/x/thorchain/types"
)

var (
	serverIP string
	port     = 1317
)

func setupConfig() {
	// Read in the configuration file for the sdk
	nw := common.GetCurrentChainNetwork()
	switch nw {
	case common.TestNet:
		fmt.Println("THORChain testnet:")
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("tthor", "tthorpub")
		config.SetBech32PrefixForValidator("tthorv", "tthorvpub")
		config.SetBech32PrefixForConsensusNode("tthorc", "tthorcpub")
		config.Seal()
	case common.MainNet:
		fmt.Println("THORChain mainnet:")
		config := cosmos.GetConfig()
		config.SetBech32PrefixForAccount("thor", "thorpub")
		config.SetBech32PrefixForValidator("thorv", "thorvpub")
		config.SetBech32PrefixForConsensusNode("thorc", "thorcpub")
		config.Seal()
	}
}

func main() {
	setupConfig()
	app := &cli.App{
		Name:   "check books",
		Usage:  "this tool will check pool balance against asgard",
		Action: checkBook,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "server",
				Aliases:  []string{"s"},
				Usage:    "server url or ip address",
				EnvVars:  []string{"SERVER_URL"},
				Value:    "localhost",
				Required: true,
				Hidden:   false,
			},
			&cli.IntFlag{
				Name:    "port",
				Aliases: []string{"p"},
				Usage:   "port",
				EnvVars: []string{"SERVER_PORT"},
				Value:   1317,
			},
			&cli.StringFlag{
				Name:     "binance",
				Aliases:  []string{"b"},
				Usage:    "binance server url host:port",
				EnvVars:  []string{"BINANCE_URL"},
				Required: true,
				Hidden:   false,
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func checkBook(ctx *cli.Context) error {
	serverIP = ctx.String("server")
	if serverIP == "" {
		return errors.New("no server ip")
	}
	port = ctx.Int("port")
	if port == 0 {
		port = 1317
	}
	binanceRPC := ctx.String("binance")
	if binanceRPC == "" {
		return errors.New("binance RPC server is empty")
	}

	pools, err := getPools()
	if err != nil {
		return err
	}

	asgards, err := getAsgard()
	if err != nil {
		return err
	}
	asgardWallets, err := getWallets(binanceRPC, asgards)
	if err != nil {
		return err
	}
	yggdrasils, err := getYggdrasil()
	if err != nil {
		return err
	}
	yggdrasilWallets, err := getWallets(binanceRPC, yggdrasils)
	if err != nil {
		return err
	}
	coins, txOutItemCount, err := getTxOutItem()
	if err != nil {
		return err
	}

	fmt.Println("current outstanding txout item:", txOutItemCount)
	printBooks(pools, asgards, yggdrasils, coins, asgardWallets, yggdrasilWallets)
	bond, err := getTotalBonds()
	if err != nil {
		return err
	}
	vaultData, err := getVaultData()
	if err != nil {
		return err
	}
	if err := printRune(pools, asgards, yggdrasils, coins, bond, vaultData, asgardWallets, yggdrasilWallets); err != nil {
		return err
	}
	return nil
}
func printRune(pools types.Pools, asgards, yggdrasils types.Vaults, coins common.Coins, bond cosmos.Uint, data types.VaultData, asgardWallets, yggdrasilWallets []common.Account) error {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"asset", "balance", "total vault balance", "total in wallets", "DIFF(pool vs Vault)", "DIFF(vault vs wallet)", "status"})
	totalBalance := cosmos.ZeroUint()
	for _, pool := range pools {
		totalBalance = totalBalance.Add(pool.BalanceRune)
	}
	totalBalance = totalBalance.Add(bond)
	totalBalance = totalBalance.Add(data.TotalReserve)
	totalBalance = totalBalance.Add(data.BondRewardRune)
	totalVault := cosmos.ZeroUint()
	for _, v := range asgards {
		c := v.Coins.GetCoin(common.RuneAsset())
		totalVault = totalVault.Add(c.Amount)
	}
	for _, v := range yggdrasils {
		c := v.Coins.GetCoin(common.RuneAsset())
		totalVault = totalVault.Add(c.Amount)
	}
	totalWallet := cosmos.ZeroUint()
	for _, v := range asgardWallets {
		for _, c := range v.Coins {
			if strings.EqualFold(c.Denom, common.RuneAsset().Symbol.String()) {
				totalWallet = totalWallet.Add(cosmos.NewUint(c.Amount))
			}
		}
	}
	for _, v := range yggdrasilWallets {
		for _, c := range v.Coins {
			if strings.EqualFold(c.Denom, common.RuneAsset().Symbol.String()) {
				totalWallet = totalWallet.Add(cosmos.NewUint(c.Amount))
			}
		}
	}
	// deduct all the RUNE suppose to be sent out
	for _, c := range coins {
		if c.Asset.Equals(common.RuneAsset()) {
			totalVault = totalVault.Sub(c.Amount)
			totalWallet = totalWallet.Sub(c.Amount)
		}
	}
	status := aurora.Green("OK")
	if !totalVault.Equal(totalBalance) {
		status = aurora.Red("FAIL")
	}
	table.Append([]string{
		common.RuneAsset().String(),
		getForDisplay(totalBalance),
		getForDisplay(totalVault),
		getForDisplay(totalWallet),
		getForDisplay(getDiff(totalVault, totalBalance)),
		getForDisplay(getDiff(totalWallet, totalVault)),
		status.String(),
	})
	table.Render()
	return nil
}
func getDiff(first, second cosmos.Uint) cosmos.Uint {
	if first.GT(second) {
		return first.Sub(second)
	}
	return second.Sub(first)
}
func printBooks(pools types.Pools, asgards, yggdrasils types.Vaults, coins common.Coins, asgardWallet, yggdrasilWallet []common.Account) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"asset", "pool balance", "total vault balance", "total in wallet", "DIFF (pool vs vault)", "DIFF (vault vs wallet)", "status"})
	for _, pool := range pools {
		totalVault := cosmos.ZeroUint()
		for _, v := range asgards {
			c := v.Coins.GetCoin(pool.Asset)
			totalVault = totalVault.Add(c.Amount)
		}
		for _, v := range yggdrasils {
			c := v.Coins.GetCoin(pool.Asset)
			totalVault = totalVault.Add(c.Amount)
		}
		c := coins.GetCoin(pool.Asset)
		totalVault = totalVault.Sub(c.Amount)
		totalWallet := cosmos.ZeroUint()
		for _, v := range asgardWallet {
			for _, c := range v.Coins {
				if strings.EqualFold(c.Denom, pool.Asset.Symbol.String()) {
					totalWallet = totalWallet.Add(cosmos.NewUint(c.Amount))
				}
			}
		}
		for _, v := range yggdrasilWallet {
			for _, c := range v.Coins {
				if strings.EqualFold(c.Denom, pool.Asset.Symbol.String()) {
					totalWallet = totalWallet.Add(cosmos.NewUint(c.Amount))
				}
			}
		}
		totalWallet = totalWallet.Sub(c.Amount)
		result := aurora.Green("OK").String()
		if !totalVault.Equal(pool.BalanceAsset) || !totalWallet.Equal(totalVault) {
			result = aurora.Red("FAIL").String()
		}
		table.Append([]string{
			pool.Asset.String(),
			getForDisplay(pool.BalanceAsset),
			getForDisplay(totalVault),
			getForDisplay(totalWallet),
			getForDisplay(getDiff(totalVault, pool.BalanceAsset)),
			getForDisplay(getDiff(totalWallet, totalVault)),
			result,
		})
	}
	table.Render() // Send output
}
func getForDisplay(input cosmos.Uint) string {
	return strconv.FormatFloat(float64(input.Uint64())/float64(common.One), 'f', -1, 64)
}
func getPools() (types.Pools, error) {
	poolsURL := fmt.Sprintf("http://%s:%d/thorchain/pools", serverIP, port)
	client := http.Client{
		Timeout: time.Second * 3,
	}
	resp, err := client.Get(poolsURL)
	if err != nil {
		return nil, fmt.Errorf("fail to get pool :%w", err)
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			fmt.Println("fail to close response body", err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("not expected status : %s", resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("fail to read response body: %w", err)
	}
	var pools types.Pools
	if err := json.Unmarshal(body, &pools); err != nil {
		return nil, fmt.Errorf("fail to unmarshal pools: %w", err)
	}
	return pools, nil
}

func getAsgard() (types.Vaults, error) {
	asgardURL := fmt.Sprintf("http://%s:%d/thorchain/vaults/asgard", serverIP, port)
	client := http.Client{
		Timeout: time.Second * 3,
	}
	resp, err := client.Get(asgardURL)
	if err != nil {
		return nil, fmt.Errorf("fail to get asgard vault :%w", err)
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			fmt.Println("fail to close response body", err)
		}
	}()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("not expected status : %s", resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("fail to read response body: %w", err)
	}
	var vaults types.Vaults

	if err := codec.Cdc.UnmarshalJSON(body, &vaults); err != nil {
		return nil, fmt.Errorf("fail to unmarshal vaults: %w", err)
	}
	return vaults, nil
}
func closeRespBody(body io.ReadCloser) {
	if err := body.Close(); err != nil {
		fmt.Println("fail to close response body", err)
	}
}
func getYggdrasil() (types.Vaults, error) {
	yggdrasilURL := fmt.Sprintf("http://%s:%d/thorchain/vaults/yggdrasil", serverIP, port)
	client := http.Client{
		Timeout: time.Second * 3,
	}
	resp, err := client.Get(yggdrasilURL)
	if err != nil {
		return nil, fmt.Errorf("fail to get yggdrasil vault :%w", err)
	}
	defer closeRespBody(resp.Body)
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("not expected status : %s", resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("fail to read response body: %w", err)
	}
	var queryVaults []types.QueryYggdrasilVaults
	if err := codec.Cdc.UnmarshalJSON(body, &queryVaults); err != nil {
		return nil, fmt.Errorf("fail to unmarshal vaults: %w", err)
	}
	var vaults types.Vaults
	for _, v := range queryVaults {
		vaults = append(vaults, v.Vault)
	}
	return vaults, nil
}

func getTxOutItem() (common.Coins, int, error) {
	outboundURL := fmt.Sprintf("http://%s:%d/thorchain/queue/outbound", serverIP, port)
	client := http.Client{
		Timeout: time.Second * 3,
	}
	resp, err := client.Get(outboundURL)
	if err != nil {
		return common.Coins{}, 0, fmt.Errorf("fail to get keysign(txout item) :%w", err)
	}
	defer closeRespBody(resp.Body)
	if resp.StatusCode != http.StatusOK {
		return common.Coins{}, 0, fmt.Errorf("not expected status : %s", resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return common.Coins{}, 0, fmt.Errorf("fail to read response body: %w", err)
	}
	var keySign []types.TxOutItem
	if err := codec.Cdc.UnmarshalJSON(body, &keySign); err != nil {
		return common.Coins{}, 0, fmt.Errorf("fail to unmarshal keysign: %w", err)
	}
	if len(keySign) == 0 {
		return common.Coins{}, 0, nil
	}
	coins := common.Coins{}
	txOutItems := 0
	for _, item := range keySign {
		if !item.OutHash.IsEmpty() {
			continue
		}
		if item.InHash.Equals(common.BlankTxID) {
			continue
		}
		txOutItems++
		if !coins.Contains(item.Coin) {
			coins = append(coins, item.Coin)
			continue
		}
		for i, c := range coins {
			if c.Asset.Equals(item.Coin.Asset) {
				coins[i].Amount = c.Amount.Add(item.Coin.Amount)
				break
			}
		}
	}
	return coins, txOutItems, nil
}

func getVaultData() (types.VaultData, error) {
	vaultURL := fmt.Sprintf("http://%s:%d/thorchain/vault", serverIP, port)
	client := http.Client{
		Timeout: time.Second * 3,
	}
	resp, err := client.Get(vaultURL)
	if err != nil {
		return types.VaultData{}, fmt.Errorf("fail to get node accounts :%w", err)
	}
	defer closeRespBody(resp.Body)
	if resp.StatusCode != http.StatusOK {
		return types.VaultData{}, fmt.Errorf("not expected status : %s", resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return types.VaultData{}, fmt.Errorf("fail to read response body: %w", err)
	}
	var vaultData types.VaultData
	if err := codec.Cdc.UnmarshalJSON(body, &vaultData); err != nil {
		return types.VaultData{}, fmt.Errorf("fail to unmarshal vault data: %w", err)
	}
	return vaultData, nil
}

func getTotalBonds() (cosmos.Uint, error) {
	nodesURL := fmt.Sprintf("http://%s:%d/thorchain/nodeaccounts", serverIP, port)
	client := http.Client{
		Timeout: time.Second * 5,
	}
	resp, err := client.Get(nodesURL)
	if err != nil {
		return cosmos.ZeroUint(), fmt.Errorf("fail to get node accounts :%w", err)
	}
	defer closeRespBody(resp.Body)
	if resp.StatusCode != http.StatusOK {
		return cosmos.ZeroUint(), fmt.Errorf("not expected status : %s", resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return cosmos.ZeroUint(), fmt.Errorf("fail to read response body: %w", err)
	}
	var nodes []types.QueryNodeAccount
	if err := codec.Cdc.UnmarshalJSON(body, &nodes); err != nil {
		return cosmos.ZeroUint(), fmt.Errorf("fail to get node accounts")
	}
	result := cosmos.ZeroUint()
	for _, node := range nodes {
		result = result.Add(node.Bond)
	}
	return result, nil
}

func getWallets(binanceUrl string, vaults types.Vaults) ([]common.Account, error) {
	var accounts []common.Account
	for _, v := range vaults {
		addr, err := v.PubKey.GetAddress(common.BNBChain)
		if err != nil {
			return nil, fmt.Errorf("fail to get BNB address: %w", err)
		}
		acct, err := getAccountByAddress(binanceUrl, addr.String())
		if err != nil {
			return nil, fmt.Errorf("fail get account detail for %s", addr)
		}
		accounts = append(accounts, acct)
	}
	return accounts, nil
}

func getAccountByAddress(binanceUrl string, accountAddr string) (common.Account, error) {
	u, err := url.Parse(binanceUrl)
	if err != nil {
		return common.Account{}, err
	}
	u.Path = "/abci_query"
	v := u.Query()
	v.Set("path", fmt.Sprintf("\"/account/%s\"", accountAddr))
	u.RawQuery = v.Encode()

	resp, err := http.Get(u.String())
	if err != nil {
		return common.Account{}, err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			log.Println("fail to close response body ", err)
		}
	}()

	type queryResult struct {
		Jsonrpc string `json:"jsonrpc"`
		ID      string `json:"id"`
		Result  struct {
			Response struct {
				Key         string `json:"key"`
				Value       string `json:"value"`
				BlockHeight string `json:"height"`
			} `json:"response"`
		} `json:"result"`
	}

	var result queryResult
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return common.Account{}, err
	}

	err = json.Unmarshal(body, &result)
	if err != nil {
		return common.Account{}, err
	}

	data, err := base64.StdEncoding.DecodeString(result.Result.Response.Value)
	if err != nil {
		return common.Account{}, err
	}

	cdc := ttypes.NewCodec()
	var acc ctypes.AppAccount
	err = cdc.UnmarshalBinaryBare(data, &acc)
	if err != nil {
		return common.Account{}, err
	}
	account := common.NewAccount(acc.BaseAccount.Sequence, acc.BaseAccount.AccountNumber, common.GetCoins(acc.BaseAccount.Coins), acc.Flags > 0)

	return account, nil
}
